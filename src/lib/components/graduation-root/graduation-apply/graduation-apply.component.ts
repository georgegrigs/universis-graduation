import {Component, OnInit} from '@angular/core';
import {ErrorService, LoadingService} from '@universis/common';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {GraduationRequestData} from '../../../graduation';
import {GraduationService} from '../../../services/graduation-request/graduation.service';

@Component({
  selector: 'universis-graduation-apply',
  templateUrl: './graduation-apply.component.html'
})
export class GraduationApplyComponent implements OnInit {

  /**
   *
   * A flag that indicates wether the current date is a graduation period for the user's department
   *
   */
  public inGraduationPeriod: boolean;

  /**
   *
   * Information regarding the graduation request
   *
   */
  public graduationRequestData: any;
  public requestPeriodExists = false;

  constructor(
    private _router: Router,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _loadingService: LoadingService,
    private graduationService: GraduationService
  ) {
  }

  async ngOnInit() {
    try {
      this._loadingService.showLoading();
      this.graduationService.getStudentRequestConfigurations().then((requestTypes) => {
        const graduationRequestAction = requestTypes.find(x => {
          return x.additionalType === 'GraduationRequestAction';
        });
        if (!graduationRequestAction) {
          this._errorService.showError(
            {
              error: 404
            });
        }
      });
      this.graduationRequestData = await this.graduationService.getGraduationRequestData();
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   *
   * Forms the title of the message box
   *
   * @param graduationRequestData The status of the graduation request
   *
   */
  getTitle(): string {
    if (this.graduationRequestData.graduationEvent) {
      const academicPeriod = this._translateService.instant(
        `UniversisGraduationModule.Semester.caps.${this.graduationRequestData.graduationEvent.graduationPeriod.alternateName}`
      );
      const semester = this._translateService.instant(`UniversisGraduationModule.Semester.caps.title`);

      return `${academicPeriod} ${semester} ${this.graduationRequestData.graduationEvent.graduationYear.alternateName}`;
    } else {
      return this._translateService.instant('UniversisGraduationModule.GraduationRequestCapitalTitle');
    }
  }

  /**
   *
   * Calculates the message of the message box
   *
   * @param graduationRequestData The status of the graduation request
   *
   */
  getMessage(): string {
    if (this.graduationRequestData.graduationRequest) {
      const graduationEvent = this.graduationRequestData.graduationRequest.graduationEvent;

      const graduationPeriod = graduationEvent.graduationPeriod.alternateName;
      const graduationYear = graduationEvent.graduationYear.alternateName;

      const status = this.graduationRequestData.graduationRequest.actionStatus.alternateName;

      if (status === 'PotentialActionStatus') {
        return this._translateService.instant('UniversisGraduationModule.PotentialGraduationRequestMessage', {
          semesterPeriod: this._translateService.instant(`UniversisGraduationModule.Semester.${graduationPeriod}`),
          academicYear: graduationYear
        });
      } else if (status === 'ActiveActionStatus') {
        return this._translateService.instant('UniversisGraduationModule.ActiveGraduationRequestMessage', {
          semesterPeriod: this._translateService.instant(`UniversisGraduationModule.Semester.${graduationPeriod}`),
          academicYear: graduationYear
        });
      } else if (status === 'CompletedActionStatus') {
        return this._translateService.instant('UniversisGraduationModule.CompletedGraduationRequestMessage', {
          semesterPeriod: this._translateService.instant(`UniversisGraduationModule.Semester.${graduationPeriod}`),
          academicYear: graduationYear
        });
      }
    } else if (this.graduationRequestData.graduationEvent) {
      const graduationPeriod = this.graduationRequestData.graduationEvent.graduationPeriod.alternateName;
      const graduationYear = this.graduationRequestData.graduationEvent.graduationYear.alternateName;

      return this._translateService.instant('UniversisGraduationModule.WithoutGraduationRequestMessage', {
        semesterPeriod: this._translateService.instant(`UniversisGraduationModule.Semester.${graduationPeriod}`),
        academicYear: graduationYear
      });
    } else {
      return this._translateService.instant('UniversisGraduationModule.WithoutGraduationEventMessage');
    }
  }

  /**
   *
   * Change the location to the graduation request
   *
   */
  public navigateToGraduationRequest() {
    this._router.navigate(['graduation/request']);
  }

  public disableButton() {
    if (!!this.graduationRequestData.graduationEvent) {
      // there is an available event
      if (!!this.graduationRequestData.graduationRequest &&
        this.graduationRequestData.graduationRequest.graduationEvent.id === this.graduationRequestData.graduationEvent.id) {
        if (this.graduationRequestData.graduationRequest.actionStatus.alternateName === 'PotentialActionStatus') {
          // there is a graduation request and it's potential
          return false;
        }
      } else if (!!this.graduationRequestData.graduationRequest &&
        this.graduationRequestData.graduationRequest.graduationEvent.id !== this.graduationRequestData.graduationEvent.id) {
        // there is a cand it's not potential
        return true;
      } else {
        // there is no request
        return false;
      }
    }
    // there is no event available
    return true;
  }
}
