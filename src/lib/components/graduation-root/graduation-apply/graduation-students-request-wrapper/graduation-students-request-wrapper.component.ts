import { Component, OnInit } from '@angular/core';
import { LoadingService } from '@universis/common';
import { GraduationRequestData } from '../../../../graduation';
import { GraduationService } from '../../../../services/graduation-request/graduation.service';

@Component({
  selector: 'universis-graduation-students-request-wrapper',
  templateUrl: './graduation-students-request-wrapper.component.html'
})
export class GraduationStudentsRequestWrapperComponent implements OnInit {

  public graduationRequestStatus: GraduationRequestData;

  constructor(
    private _graduationRequestService: GraduationService,
    private _loadingService: LoadingService
  ) { }

  async ngOnInit() {
    try {
      this._loadingService.showLoading();
      this.graduationRequestStatus = await this._graduationRequestService.getGraduationRequestData();
    } catch (err) {
      console.error('graduation: ', err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

}
