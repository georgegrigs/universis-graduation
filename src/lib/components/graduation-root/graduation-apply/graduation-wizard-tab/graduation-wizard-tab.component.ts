import { Component, OnInit, Input } from '@angular/core';
import { GraduationRequestData } from '../../../../graduation';
import { BehaviorSubject, Subscription, Observable  } from 'rxjs';

@Component({
  template: ''
})
export class GraduationWizardTabComponent {


  /**
   * @property {string} alternateName A unique identifier for the graduation process step
   */
  protected alternateName: string;

  /**
   * @property {BehaviorSubject<any>} graduationRequestStatusObservable$
   * Handles the communication with the parent component
   */
  @Input() graduationRequestStatusObservable$: BehaviorSubject<any>;

  /**
   * @property {Subscription} graduationRequestStatusSubscription
   * The overall graduation request status
   */
  graduationRequestStatusSubscription: Subscription;

  /**
   * @property {GraduationRequestData} graduationRequestStatus The overall graduation request status
   */
  @Input() graduationRequestData: GraduationRequestData;

  getAlternateName() {
    return this.alternateName;
  }

  /**
   *
   * Retrieves the step object from the graduation  request status
   *
   * @param graduationRequestStatus The graduation request status data
   *
   */
  getStep(graduationRequestStatus: any) {
    return graduationRequestStatus.steps.find(
      step => new RegExp(this.alternateName, 'gi').test(step.alternateName)
    );
  }
}
