import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationApplyComponent } from './graduation-apply.component';

describe('GraduationStudentsGraduationTabComponent', () => {
  let component: GraduationApplyComponent;
  let fixture: ComponentFixture<GraduationApplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationApplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationApplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
