import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationDocumentSubmissionComponent } from './graduation-document-submission.component';

describe('GraduationDocumentSubmissionComponent', () => {
  let component: GraduationDocumentSubmissionComponent;
  let fixture: ComponentFixture<GraduationDocumentSubmissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationDocumentSubmissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationDocumentSubmissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
