import { Component, OnInit, OnDestroy } from '@angular/core';
import { GraduationWizardTabComponent } from '../../../graduation-wizard-tab/graduation-wizard-tab.component';

@Component({
  selector: 'universis-graduation-ceremony',
  templateUrl: './graduation-ceremony.component.html'
})
export class GraduationCeremonyComponent extends GraduationWizardTabComponent implements OnInit, OnDestroy {

  constructor() {
    super();
  }

  /**
   * A list of certificates for the student.
   */
  public certificates: Array<any>;

  /**
   * A message for the student in case of failed graduation
   */
  public message: string;

  public currentStep;

  ngOnInit() {
    this.alternateName = 'graduationCeremony';

    if (this.graduationRequestData) {
      this.currentStep = this.graduationRequestData.steps.filter(step => {
        if (step && step.alternateName === 'graduationCeremony') {
          return step;
        }
      });
      this.fillData();
    } else {
      this.graduationRequestStatusObservable$.subscribe((graduationData) => {
        this.graduationRequestData = graduationData;
        this.currentStep = this.getStep(this.graduationRequestData);
        this.fillData();
      });
    }
  }

  fillData() {
    if (this.currentStep) {
      return;
    }

    if (this.currentStep.data) {
      this.certificates = this.currentStep.data.certificates;
    }
  }

  ngOnDestroy() {
    if (this.graduationRequestStatusSubscription) {
      this.graduationRequestStatusSubscription.unsubscribe();
    }
  }

}
