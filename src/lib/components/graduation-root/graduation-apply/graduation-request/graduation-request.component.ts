import {
  Component,
  OnInit,
  OnDestroy,
  ViewContainerRef,
  ComponentFactoryResolver,
  ViewChild,
  AfterViewInit,
  TemplateRef
} from '@angular/core';
import { Subscription, BehaviorSubject } from 'rxjs';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import { GraduationService } from '../../../../services/graduation-request/graduation.service';
import { GraduationItemStatus, GraduationRequestData } from '../../../../graduation';
import {Router} from '@angular/router';

@Component({
  selector: 'universis-graduation-request',
  templateUrl: './graduation-request.component.html',
  styles: [`

    .fa-ban:before {
      background-color: #d0d0d0;
      padding: 2px;
      border-radius: 20px;
    }

  `]
})
export class GraduationRequestComponent implements AfterViewInit, OnDestroy {
  // This component is based on this example
  // https://angular.io/guide/dynamic-component-loader

  @ViewChild('GraduationRequestWizardDirective', {read: ViewContainerRef}) GraduationRequestWizard;

  /**
   * The observable that holds the graduation status
   */
  public graduationStatusAsObservable$: BehaviorSubject<any>;

  /**
   * The form submission for the graduation form
   */
  private currentStepOutput: Subscription;

  /**
   * The list of the wizard steps
   */
  public wizardSteps: any;

  /**
   * The current active step index
   */
  public currentAdIndex = -1;

  /**
   * The current active step object
   */
  public activeStep;

  public isLoading = true;

  modalRef;
  @ViewChild('ConfirmModalTemplate') modalTemplate: TemplateRef<any>;

  constructor(
    private graduationService: GraduationService,
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _router: Router
  ) { }

  public graduationRequestData: GraduationRequestData;

  async ngAfterViewInit() {
    try {
      this._loadingService.showLoading();
      this.graduationService.getStudentRequestConfigurations().then ( (requestTypes) => {
        const graduationRequest =  requestTypes.find ( x => {
          return x.additionalType === 'GraduationRequestAction';
        });
        if (!graduationRequest) {
          this._errorService.showError(
            {
              error: 404
            });
        }
      });
      this.graduationStatusAsObservable$ = new BehaviorSubject(undefined);
      await this.updateRequestStatusData();
      this.loadComponent(0, 'front');
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }

  ngOnDestroy() {
    if (this.currentStepOutput) {
      this.currentStepOutput.unsubscribe();
    }
  }

  /**
   *
   * Updates the graduation request status
   *
   * @param newStatus
   *
   */
  private async updateRequestStatusData(graduationRequestData?: any): Promise<void> {
    try {
      if (graduationRequestData) {
        this.graduationRequestData = graduationRequestData;
      } else {
        this.graduationRequestData = await this.graduationService.getGraduationRequestData();
      }

      this.graduationStatusAsObservable$.next(this.graduationRequestData);
    } catch (err) {
      console.error(err);
    }
  }

  /**
   *
   * Shows the component (step) at the wizard
   *
   * @param index The index inside the steps array to show
   * direction : 'front' , 'back'
   *
   */
  loadComponent(index: number, direction: string): void {
    if (index < 0 || index >= this.graduationRequestData.steps.length ) {
      return;
    } else if (this.graduationRequestData.steps[index].status === 'unavailable') {
      if (direction === 'front') {
        this.loadComponent(index + 1, direction);
      } else {
        this.loadComponent(index - 1, direction);
      }
      return;
    }

    this._loadingService.showLoading();
    this.isLoading = true;
    if (this.currentStepOutput) {
      this.currentStepOutput.unsubscribe();
    }

    const component = this.graduationRequestData.steps[index].component.component;
    this.activeStep = this.graduationRequestData.steps[index];
    this.currentAdIndex = index;
    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(component);
    this.GraduationRequestWizard.clear();
    const currentComponent = this.GraduationRequestWizard.createComponent(componentFactory);
    currentComponent.instance.graduationRequestStatusObservable$ = this.graduationStatusAsObservable$;
    currentComponent.changeDetectorRef.detectChanges();

    // special configuration for components
    if (this.activeStep.alternateName === 'graduationRequest') {
      this.currentStepOutput = currentComponent.instance.formSubmit.subscribe((value) => this.submitGraduationForm(value));
    }

    if (this.activeStep.alternateName === 'graduationDocumentsSubmission') {
      this.currentStepOutput = currentComponent.instance.outputAction.subscribe(
        async (event) => this.handleDocumentsSubmissionStepOutput(event)
      );
    }
    this._loadingService.hideLoading();
    this.isLoading = false;
  }

  /**
   *
   * Submits the graduation form
   *
   * @param value The graduation request form submission
   *
   */
  async submitGraduationForm(data) {
    try {
      this.isLoading = true;
      this._loadingService.showLoading();
      const newStatus = await this.graduationService.setGraduationRequest(data.graduationEventId, data.form);
      await this.updateRequestStatusData(newStatus);
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }

  /**
   *
   * Handles the document submission events
   *
   * @param event The data passed by the documents submission step
   *
   */
  async handleDocumentsSubmissionStepOutput(event) {
    this._loadingService.showLoading();
    this.isLoading = true;
    try {
      let newStatus;
      if (event.action === 'upload') {
        newStatus = await this.graduationService.uploadGraduationRequestAttachment(event.data);
      } else if (event.action === 'remove') {
        newStatus = await this.graduationService.removeGraduationRequestAttachment(event.data);
      } else if (event.action === 'download') {
        newStatus = await this.graduationService.downloadGraduationRequestAttachment(event.data);
      }
      await this.updateRequestStatusData(newStatus);
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }

  openConfirmModal() {
    this.modalRef = this._modalService.openModal(this.modalTemplate);
  }

  closeConfirmModal() {
    this.modalRef.hide();
  }

  async completeRequest() {
    this._loadingService.showLoading();
    this.isLoading = true;
    try {
      await this.graduationService.setGraduationRequestActiveStatus(this.graduationRequestData);
      this._router.navigateByUrl('graduation/apply');
    } catch (err) {
      console.error(err);
    } finally {
      this.closeConfirmModal();
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }


}
