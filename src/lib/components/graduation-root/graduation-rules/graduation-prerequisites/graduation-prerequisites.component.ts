import {Component, Input, OnInit} from '@angular/core';
import { LoadingService } from '@universis/common';
import { GraduationService } from '../../../../services/graduation-request/graduation.service';

@Component({
  selector: 'universis-graduation-prerequisites',
  templateUrl: './graduation-prerequisites.component.html',
  styleUrls: ['./graduation-prerequisites.component.scss']
})
export class GraduationPrerequisitesComponent implements OnInit {

  public courseTypes: any;
  public isLoading = true;
  public graduationInfo: any;
  public studentGuide: any;
  public studentSpecialty: any;
  @Input('student') student: any;

  constructor(private _loadingService: LoadingService,
              private _graduationRequestService: GraduationService) {}

  async ngOnInit() {
    try {
      this._loadingService.showLoading();
      const getStudent = this._graduationRequestService.getStudent(this.student);
      const getGraduationRules = this._graduationRequestService.getGraduationRules(this.student);
      const getCourseTypes = this._graduationRequestService.getCourseTypes();
      const [student, graduationInfo, courseTypes] = await Promise.all([getStudent, getGraduationRules, getCourseTypes]);

      this.courseTypes = courseTypes;
      this.graduationInfo = graduationInfo;
      this.studentGuide = student.studyProgram.name;
      this.studentSpecialty = student.specialty;
    } catch (err) {
      console.warn(err);
    } finally {
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }
}
