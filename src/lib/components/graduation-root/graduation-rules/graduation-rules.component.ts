import { Component, OnInit } from '@angular/core';
import {LoadingService} from '@universis/common';
import {GraduationService} from '../../../services/graduation-request/graduation.service';
import { Router} from '@angular/router';

@Component({
  selector: 'lib-graduation-rules',
  templateUrl: './graduation-rules.component.html',
  styleUrls: ['./graduation-rules.component.scss']
})
export class GraduationRulesComponent implements OnInit {

  private GRADUATIONPREREQUISITES = 'graduationPrerequisites';
  private GRADUATIONPROGRESS = 'graduationProgress';

  public isLoading = true;
  public graduationInfo: any;
  public studentGuide: any;
  public studentSpecialty: any;
  public selectedType = '';

  constructor(private _loadingService: LoadingService,
              private _graduationRequestService: GraduationService,
              private _router: Router) {}

  async ngOnInit() {
    try {
      this.selectedType = this.getCurrentPath();

      this._loadingService.showLoading();
      const getStudent = this._graduationRequestService.getStudent('me');
      const getGraduationRules = this._graduationRequestService.getGraduationRules('me');
      const [student, graduationInfo] = await Promise.all([getStudent, getGraduationRules]);

      this.graduationInfo = graduationInfo;
      this.studentGuide = student.studyProgram.name;
      this.studentSpecialty = student.specialty;
    } catch (err) {
      console.warn(err);
    } finally {
      this._loadingService.hideLoading();
      this.isLoading = false;
    }
  }

  navigate(url) {
    this.selectedType = url;
    this._router.navigateByUrl('graduation/prerequisites/' + url);
  }

  getCurrentPath() {
    const size = this._router.url.split('/').length;
    const lastPath = this._router.url.split('/')[size - 1];
    return lastPath;
  }
}
