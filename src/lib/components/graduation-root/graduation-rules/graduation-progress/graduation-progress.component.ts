import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { LoadingService } from '@universis/common';
import { GraduationService } from '../../../../services/graduation-request/graduation.service';


@Component({
  selector: 'universis-graduation-progress',
  templateUrl: './graduation-progress.component.html',
  styleUrls: ['./graduation-progress.component.scss']
})
export class GraduationProgressComponent implements OnInit, OnChanges {
  public messageRes: void;
  public courseTypes: any;
  public studentSpecialty: any;
  public studentGuide: any;
  public isLoading = true;
  public graduationInfo: any;
  @Input('student') student: any;

  constructor(private _loadingService: LoadingService,
              private _graduationRequestService: GraduationService) {}

  async ngOnInit() {

  }

    ngOnChanges(changes: SimpleChanges): void {
    if (changes.student) {
      if (changes.student.currentValue == null) {
        this.student = null;
        return;
      }
      (async () => {
        try {
          this._loadingService.showLoading();
          this.student = changes.student.currentValue;
          const getGraduationRules = this._graduationRequestService.getGraduationRules(this.student);
          const getCourseTypes = this._graduationRequestService.getCourseTypes();
          const [graduationInfo, courseTypes] = await Promise.all([getGraduationRules, getCourseTypes]);
          this.courseTypes = courseTypes;
          this.graduationInfo = graduationInfo;
          this._loadingService.hideLoading();
          this.isLoading = false;
        } catch (err) {
          this._loadingService.hideLoading();
          this.isLoading = false;
          console.warn(err);
        }
      })().then(() => {
        //
      }).catch((err) => {
        console.warn(err);
      });
    }
  }


  isNumber(value) {
    return typeof value === 'number';
  }

}
