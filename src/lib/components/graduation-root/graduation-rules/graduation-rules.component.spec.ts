import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationRulesComponent } from './graduation-rules.component';

describe('GraduationPrerequisitesComponent', () => {
  let component: GraduationRulesComponent;
  let fixture: ComponentFixture<GraduationRulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationRulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
