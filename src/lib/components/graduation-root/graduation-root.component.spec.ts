import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationRootComponent } from './graduation-root.component';

describe('GraduationRootComponent', () => {
  let component: GraduationRootComponent;
  let fixture: ComponentFixture<GraduationRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
